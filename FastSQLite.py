"""
Copyright (c) 2016 Giulio Ganzerli. All rights reserved.
Use of this source code is governed by a CC BY-NC 4.0 license that can
be found in the LICENSE file.
"""

import sqlite3
import os
import platform

class FastSQLite:
    
    
    def __init__(self, db_name = "", table_name = ""):
        """
        Takes variables to create a connection to an existing 
        database. If it doesn't exist then no connection is created.
        Providing an invalid database name will result in error.
        If the connection is successfully created, then
        values can be inserted/updated or retrieved using the provided
        methods.
        
        Keyword arguments:
        db_name -- name of the target existing database
        table_name -- name of the table to load
        """
        
        self.db_name = db_name
        self.table_name = table_name
        self.folder = os.getcwd()
        self.platform = platform.system()
        if db_name != "":               
            self.middle = '\\'
            if self.platform == 'Linux':
                self.middle = '/'
            self.connection = sqlite3.connect(self.folder +
                                              self.middle +
                                              self.db_name)
            self.database = self.connection.cursor()
        else:
            pass;
    
    def close(self):
        self.connection.close()
    
    def retrieve(self, key):
        """
        Takes a key and returns the corresponding values in the
        database.
        
        Keyword arguments:
        key -- the key of the needed value
        """
        
        self.database.execute('SELECT Value FROM ' 
                              + self.table_name +
                              ' WHERE Name = ?',
                              (str(key),))
        for self.results in self.database:
            return self.results[0]
            
    def insert(self, key, value):
        """
        Takes a key and a value: if the key already exists then
        it's updated. If not it's created.
        
        Keyword arguments:
        key -- the key of the value
        value -- the value to insert/update
        """
        
        if self.retrieve(key) == None:
            self.database.execute('INSERT INTO '
                                  + self.table_name +
                                  '(Name, Value) VALUES (?, ?)',
                                  (str(key), value))
        else:
            self.database.execute('UPDATE '
                                  + self.table_name +
                                  ' SET Value = ? WHERE Name = ?',
                                  (value, key))
        self.connection.commit()       
