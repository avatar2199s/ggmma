#!/usr/bin/env python3
"""
Copyright (c) 2016 Giulio Ganzerli. All rights reserved.
Use of this source code is governed by a CC BY-NC 4.0 license that can
be found in the LICENSE file.
""" 
import PySide.QtGui as QtGui
import PySide.QtCore as QtCore
import FastSQLite
import psutil
import sys
import os      

class GGMMA:


    def __init__(self):

        
        self.load()
        self.app = QtGui.QApplication(sys.argv)
        self.app.setWindowIcon(QtGui.QIcon('Icon.ico'))
        
        # Create main window and set background
        self.window = QtGui.QWidget()
        self.window.setWindowTitle('GGMMA')
        self.window.setWindowIcon(QtGui.QIcon('Icon.ico'))
        self.window.setAutoFillBackground(True)
        p = self.window.palette()
        p.setColor(self.window.backgroundRole(), QtGui.QColor('#EEEEEE'))
        self.window.setPalette(p)
        
        # Create Layout, widgets and apply them
        self.grid = QtGui.QGridLayout()
        self.create()
        self.window.setLayout(self.grid)
        self.window.show()
        
        # Start methods
        self.update_ram_data()
        self.check_state()
        
        self.timer = QtCore.QTimer(self.window)
        self.timer.timeout.connect(self.update_ram_data)
        self.timer.start(250)
        
        if self.toggle_state == True:
            if self.amount_selection.value() < float(self.available_ram_data):
                self.launch(True)
            else:
                self.toggle_auto.setChecked(False)
                self.save_settings()
        
        self.app.exec_()
    
    def load(self):
        
        self.db = FastSQLite.FastSQLite("settings.db", "settings_table")
        self.path = self.db.retrieve('path')
        self.to_allocate = self.db.retrieve('to_allocate')
        self.input_style = self.db.retrieve('input_style')
        self.button_enabled_style = self.db.retrieve('button_enabled_style')
        self.button_disabled_style = self.db.retrieve('button_disabled_style')
        self.secondaries_style = self.db.retrieve('secondaries_style')
        self.spin_style = self.db.retrieve('spin_style')
        self.toggle_style = self.db.retrieve('toggle_style')
        self.text_style = self.db.retrieve('text_style')
        self.text_style_error = self.db.retrieve('text_style_error')
        self.toggle_state = self.db.retrieve('toggle_state')
        self.db.close()
        
    def create(self):
        
        self.input = []
        
        # Validator for path  entry
        path_regex = QtCore.QRegExp('^([A-Z]{1}[:]{1}[/]{1})(.*|.*[/])*\.jar$')
        path_validator = QtGui.QRegExpValidator(path_regex)
        
        # Creation of the path entry
        self.path_input = QtGui.QLineEdit()
        self.path_input.setStyleSheet(str(self.input_style))
        self.path_input.setValidator(path_validator)
        self.path_input.setPlaceholderText(
            'Complete path of your jar launcher')
        self.path_input.setMinimumWidth(240)
        if self.path != None:
            self.path_input.setText(str(self.path))
        self.path_input.textChanged.connect(self.check_state)
        self.grid.addWidget(self.path_input, 0, 0)
        
        self.input.append(self.path_input)
        
        self.browse = QtGui.QPushButton()
        self.browse.setText("Browse")
        self.browse.setStyleSheet(str(self.secondaries_style))
        self.browse.clicked.connect(self.browser)
        self.grid.addWidget(self.browse, 0, 1)
        
        self.sub_grid = QtGui.QGridLayout()
        self.grid.addLayout(self.sub_grid, 1, 0, 3, 2)
        
        self.amount_selection = QtGui.QSpinBox()
        self.amount_selection.setMinimum(1)
        self.amount_selection.setMaximum(16)
        self.amount_selection.setSingleStep(1)
        self.amount_selection.setSuffix(' GBs')
        self.amount_selection.setPrefix('    ')
        self.amount_selection.setValue(self.to_allocate)
        self.amount_selection.setStyleSheet(str(self.spin_style))
        self.amount_selection.setFixedWidth(125)
        self.amount_selection.valueChanged.connect(self.check_state)
        self.sub_grid.addWidget(self.amount_selection, 0, 0)
        
        self.input.append(self.amount_selection)
    
        self.toggle_auto = QtGui.QCheckBox()
        self.toggle_auto.setText('Autolaunch')
        self.toggle_auto.setChecked(self.toggle_state)
        self.toggle_auto.setStyleSheet(str(self.toggle_style))
        self.sub_grid.addWidget(self.toggle_auto, 1, 0)
        
        self.launch_button = QtGui.QPushButton()
        self.launch_button.setText('LAUNCH\nMINECRAFT')
        self.launch_button.setEnabled(False)
        self.launch_button.setStyleSheet(str(self.button_disabled_style))
        self.launch_button.clicked.connect(self.launch)
        self.sub_grid.addWidget(self.launch_button, 0, 1, 2, 2)
        
        self.total_ram = QtGui.QLabel()
        self.total_ram.setStyleSheet(self.text_style)
        self.total_ram.setFixedWidth(200)
        self.grid.addWidget(self.total_ram, 0, 2)
        
        self.available_ram = QtGui.QLabel()
        self.available_ram.setStyleSheet(self.text_style)
        self.available_ram.setFixedWidth(200)
        self.grid.addWidget(self.available_ram, 1, 2)
        
        self.save_button = QtGui.QPushButton()
        self.save_button.setText('Save Settings')
        self.save_button.clicked.connect(self.save_settings)
        self.save_button.setStyleSheet(self.secondaries_style)
        self.save_button.setFixedWidth(180)
        self.grid.addWidget(self.save_button, 2, 2)
        
        self.update_ram_data()
    
    def update_ram_data(self):    
            
        self.ram_data = psutil.virtual_memory()
        self.total_ram_data = str(round(self.ram_data[0]/1073741824, 2))
        self.available_ram_data = str(round(self.ram_data[1]/1073741824, 2))
        self.total_sentence = ('Total RAM: ' +
                               self.total_ram_data +
                               'GBs')
        self.available_sentence = ('Available: ' +
                                   self.available_ram_data +
                                   'GBs')
        
        self.total_ram.setText(self.total_sentence)
        self.available_ram.setText(self.available_sentence)
        
    def browser(self):
        
        self.file_browser = QtGui.QFileDialog()
        self.file_browser.setFilter("Java Archives (*.jar)")
        self.file_browser.setFileMode(QtGui.QFileDialog.ExistingFile)
        dialog_request = self.file_browser.getOpenFileName
        path = dialog_request(self.window,
                              'Select the path of your jar launcher',
                              '',
                              'Java Archive (*.jar)')
        path = str(path[0])
        if path != "":
            self.path_input.setText(path)
        else:
            pass

    def check_state(self):
        
        counter = 0
        item1 = self.input[0]
        state = item1.validator().validate(item1.text(), 0)[0]
        if state == QtGui.QValidator.Acceptable:
            counter += 1
        item2 = self.input[1]
        selected = item2.value()    
        if selected < float(self.available_ram_data):
            counter += 1
            self.available_ram.setStyleSheet(self.text_style)
        else:
            self.available_ram.setStyleSheet(self.text_style_error)
        if counter == len(self.input):
            self.launch_button.setEnabled(True)
            self.launch_button.setStyleSheet(self.button_enabled_style)
        else:
            self.launch_button.setEnabled(False)
            self.launch_button.setStyleSheet(self.button_disabled_style)
    
    def delay(self, to_wait):
        
        die_time = QtCore.QTime.currentTime().addMSecs(to_wait)
        while QtCore.QTime.currentTime() < die_time:
            self.app.processEvents(QtCore.QEventLoop.AllEvents, 100)
    
    def launch(self, minimize = False):
        
        path = str(self.path_input.text())
        value = int(self.amount_selection.value())*1024
        db = FastSQLite.FastSQLite('settings.db', 'settings_table')
        if db.platform == 'Windows':
            command = 'javaw -Xmx{0}m -Xms{0}m -jar {1}'.format(value,
                                                                path)
        elif db.platform == 'Linux':
            command = 'java -Xmx{0}m -Xms{0}m -jar {1}'.format(value,
                                                               path)
        os.popen(command)
        self.launch_button.setText('LAUNCHING...')
        self.launch_button.setEnabled(False)
        self.delay(1500)
        self.launch_button.setText('MINECRAFT\nLAUNCHED')
        self.delay(1000)
        if minimize == False:
            self.app.quit()
        else:
            self.window.setWindowState(QtCore.Qt.WindowMinimized)
    
    def save_settings(self):
        
        GB = self.amount_selection.value()
        path = str(self.path_input.text())
        toggled = self.toggle_auto.isChecked()
        self.db = FastSQLite.FastSQLite('settings.db', 'settings_table')
        self.db.insert('to_allocate', GB)
        self.db.insert('path', path)        
        self.db.insert('toggle_state', toggled)
        self.db.close()

if __name__ == "__main__":       
    x = GGMMA()
