===============================================================================
Giulio Ganzerli's Minecraft Memory Allocator - GGMMA README File
===============================================================================

================================ SUMMARY ======================================
1. Version
2. The software
3. Set up (binaries)
3.1 Compatibility
3.2 Download and use installer
3.3 Note on direct use of python script
3.4 Requirements of python script
3.5 Download and use of python script
4. Contribution guidelines
5. Contacts
6. Set up of Python environmental variables (Windows)


1. Giulio Ganzerli's Minecraft Memory Allocator - Version 1.0.1 Stable

2. GGMMA allows to allocate more memory to the Java Virtual Machine easily.
   This is especially useful when you're running a Minecraft installation with
   a lot of mods or a very heavy World with high graphic settings. It allows
   you. It should be opened before of the launcher.
   
================================= 3. SET UP =====================================

3.1 GGMMA is actually compatible with 64bit windows only. 32bit Windows should 
	work if you direcly run python script.
	
	NOTE that the software can be used with .jar launchers only. The standard
	Minecraft launcher isn't supported but it has in-built memory allocation
	settings, so the use of GGMMA is intended for legacy minecraft versions.
	
3.2	The easiest way to install GGMMA is to download the latest version's 
	installer direcly from BitBucket at:
		- https://bitbucket.org/avatar2199s/ggmma/downloads/
	
	NOTE that there are actually binaries available only for 64bit Windows.
	
	NOTE that antiviruses may block both the installer and the software at
	the first execution, however this software doesn't contain any malicious
	software. If you want, you can check the souce code.
	
	Just double-click the installer and follow the procedure. Once the
	install has finished, download the .jar launcher from:
	- https://mega.nz/#!pFs2HSoC
	or your own, if you have one, but remember I cannot guarantee any
	compatibility.
	
3.3	You may also download the source and use it as-is. But, if you will,
	you must first satisfy some requirements.
	Check point 3.4 and 3.5 for more info.

3.4 For the direct use of python script, you must have:
		- A working python 3+ installation, with PATH set (check point 6 
		  for more info);
		- psutil 4.3.0+;
		- PySide
		- A .jar minecraft launcher (you can download it at:
			- https://mega.nz/#!pFs2HSoC)
		  or use your own, if you have one, though I cannot
		  guarantee any compatibility.
		  
	
3.5 From the link:
		- https://bitbucket.org/avatar2199s/ggmma/downloads/
	click "download repository", unzip it in a folder and
	do

4. If you wish to help me, just create a pull request at:
	- https://bitbucket.org/avatar2199s/ggmma/pull-requests/
   writing the code you want to change or the function you want to add.
   I'll be happy to discuss to solve code problems and improve the quality.
   If you simply want to share an issue report, you can do it at:
    - https://bitbucket.org/avatar2199s/ggmma/issues?status=new&status=open

5. For anything else, contact me on my email address:
	- ganzerli.giulio.99@gmail.com
	
6. If you have installed Python using the standard installer, Python
   should have been already added to the system PATH. However, to 
   check, hit Windows button + R. Then write in the small window
   'cmd' and hit Enter. Insert 'python' and press Enter again.
   If the Python interpreter starts you should see:
    - Python 3.4.1 (v3.4.1:c0e311e010fc, May 18 2014, 10:45:13)
      [MSC v.1600 64 bit (AMD64)] on win32 Type "help", "copyright",
	  "credits" or "license" for more information.
	   >>>
	If that happens, the PATH is already set. If it isn't:
	 - Open 'Computer'. In the white area of the window right-click
	   and select 'Properties'. Then open 'Advanced Systems Settings'
	   from the left menu. Click on 'enviroment variables' and in
	   System Variables find PATH or create it. Then insert the path to
	   your Python installation directory. (more advanced tutorials
	   available on youtube)
	   